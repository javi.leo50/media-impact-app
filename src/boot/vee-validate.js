import Vue from 'vue';
import es from 'vee-validate/dist/locale/es';
import VeeValidate, { Validator } from 'vee-validate';
Vue.use(VeeValidate, {
    delay: 100,
    errorBagName: 'errorsvvalidate', // change if property conflicts
    inject: true,
    locale: 'es',
});

Validator.localize('es', es);
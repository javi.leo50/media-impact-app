import { Notify } from 'quasar'

const validateImageModule = {
    validateImageDropzone: function (
        file,
        ref,
        maxFile,
        maxFileSize,
        maxFileSizeText
    ) {
        if (!/\.(jpg|jpeg|png|svg)$/i.test(file.name)) {
            Notify.create({
                color: 'negative',
                position: 'bottom',
                message: ' Debe seleccionar una imagen svg, jpg, jpeg o png ',
                icon: 'report_problem'
            })

            ref.files.pop();
            if (
                file.previewElement.parentNode.classList.contains("dz-started") &&
                ref.files < 1
            ) {
                file.previewElement.parentNode.classList.remove("dz-started");
            }
            if (file.previewElement)
                file.previewElement.parentNode.removeChild(file.previewElement);
        }
        if (file.size > maxFileSize) {
            Notify.create({
                color: 'negative',
                position: 'bottom',
                message: "Debe seleccionar una imagen menor a " + maxFileSizeText,
                icon: 'report_problem'
            })
            if (ref.files.length) {
                ref.files.pop();
            }
            if (
                file.previewElement.parentNode.classList.contains("dz-started") &&
                ref.files < 1
            ) {
                file.previewElement.parentNode.classList.remove("dz-started");
            }
            file.previewElement.parentNode.removeChild(file.previewElement);
        }
        if (ref.files.length > maxFile) {
            Notify.create({
                color: 'negative',
                position: 'bottom',
                message: "Solo puede subir " + maxFile + " archivo",
                icon: 'report_problem'
            })

            ref.files.pop();
            if (ref.files.length)
                file.previewElement.parentNode.removeChild(file.previewElement);
        }
    },
};

const validateFileModule = {
    validateFileDropzone: function (
        file,
        ref,
        maxFile,
        maxFileSize,
        maxFileSizeText
    ) {
        if (!/\.(doc|docx|pdf)$/i.test(file.name)) {
            Notify.create({
                color: 'negative',
                position: 'bottom',
                message: ' Debe seleccionar un archivo doc, docx, o pdf ',
                icon: 'report_problem'
            })

            ref.files.pop();
            if (
                file.previewElement.parentNode.classList.contains("dz-started") &&
                ref.files < 1
            ) {
                file.previewElement.parentNode.classList.remove("dz-started");
            }
            if (file.previewElement)
                file.previewElement.parentNode.removeChild(file.previewElement);
        }
        if (file.size > maxFileSize) {
            Notify.create({
                color: 'negative',
                position: 'bottom',
                message: "Debe seleccionar un archivo menor a " + maxFileSizeText,
                icon: 'report_problem'
            })
            if (ref.files.length) {
                ref.files.pop();
            }
            if (
                file.previewElement.parentNode.classList.contains("dz-started") &&
                ref.files < 1
            ) {
                file.previewElement.parentNode.classList.remove("dz-started");
            }
            file.previewElement.parentNode.removeChild(file.previewElement);
        }
        if (ref.files.length > maxFile) {
            Notify.create({
                color: 'negative',
                position: 'bottom',
                message: "Solo puede subir " + maxFile + " archivo",
                icon: 'report_problem'
            })

            ref.files.pop();
            if (ref.files.length)
                file.previewElement.parentNode.removeChild(file.previewElement);
        }
    },
};
export default ({ Vue }) => {
    Vue.prototype.$validate_image = validateImageModule;
    Vue.prototype.$validate_file = validateFileModule;
}
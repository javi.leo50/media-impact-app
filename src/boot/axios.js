
import axios from 'axios'
import { Notify, Loading } from 'quasar'
//axios.defaults.baseURL = process.env.API; 
let url = process.env.BASE_URL_API;
let cookie = true;
const axiosInstance = axios.create({ baseURL: url })
//const axiosInstance = axios.create()

export default async ({ router, store, Vue }) => {
    Loading.show()
    // Request interceptor
    axiosInstance.interceptors.request.use(request => {
        const token = store.getters['user/tokenGetter']
        if (token) request.headers.common['Authorization'] = `Bearer ${token}`
        return request
    })

    // Response interceptor
    axiosInstance.interceptors.response.use(response => response, error => {
        const { status } = error.response

        if (status >= 500) {
            Notify.create({
                color: 'negative',
                position: 'bottom',
                message: error.response.data.error,
                icon: 'report_problem'
            })
        }

        if (status === 404 && store.getters['user/authGetter']) {
            if (error.response.config.responseType == 'arraybuffer') {
                Notify.create({
                    color: "success",
                    message: 'Imagen o Archivo solicitado no existe.',
                })
            } else {
                Notify.create({
                    color: "success",
                    message: error.response.data.error,
                })
            }
        }

        if (status === 401 && store.getters['user/authGetter']) {
            store.dispatch('user/logoutActionWithInvalidToken').then(() => {
                Notify.create({
                    color: 'negative',
                    position: 'bottom',
                    message: error.response.data.error,
                    icon: 'report_problem'
                })
            })
        }
        if (status === 403) {
            router.push({ name: "error", params: { valor: "2" } }).then(() => {
                Notify.create({
                    color: 'negative',
                    position: 'bottom',
                    message: error.response.data.error,
                    icon: 'report_problem'
                })
            })
        } return Promise.reject(error)
    })

    let auth = []
    auth = await store.dispatch('user/authAction').catch(() => {
        store.commit('user/logoutMutation')
    }) // Auth CheCK

    Vue.prototype.$axios = axiosInstance;

    router.beforeEach(async (to, from, next) => {
        if (store.getters['user/tokenGetter']) { // Authenticated Router 
            if (to.name == 'login') {
                Loading.hide();
                next({ path: '/dashboard' });
            } else {
                const requiresAuth = to.matched.some(route => route.meta.requiresAuth)
                if (requiresAuth) {

                    if (!auth) {
                        auth = await store.dispatch('user/authAction').catch(() => {
                            store.commit('user/logoutMutation')
                        }) // Auth Check  
                    }
                    if (auth) {
                        let filtered = auth.data.modules;
                        let filtered_validar_hijos = auth.data.modules;
                        let filtered_hijos = auth.data.modules;

                        filtered = auth.data.modules.filter((e) =>
                            e.nameref == to.name
                        );
                        filtered_validar_hijos = auth.data.modules.filter((e) =>
                            e.parent
                        );
                        filtered_hijos = filtered_validar_hijos.filter((e) =>
                            e.q_subestados.some((q) =>
                                e.nameref + '.' + q == to.name
                            )
                        );
                        if (filtered.length < 1 && filtered_hijos.length < 1) { //si es mayor a 1 es que si tiene permisos
                            Notify.create({
                                color: 'negative',
                                position: 'bottom',
                                message: 'Oops. No tiene permisos suficientes...',
                                icon: 'report_problem'
                            })
                            Loading.hide();
                            next({ name: "error", params: { valor: "2" } })
                        } else {
                            Loading.hide();
                            next()
                        }
                    }


                } else {
                    Loading.hide();
                    next()
                }
            }
        } else {
            Loading.hide();
            auth = "";
            next(!to.meta.requiresAuth || { path: '/login' })
        }
    })
}
export { axiosInstance, cookie }






const routes = [
  {
    path: '/',
    redirect: {
      name: 'login'
    }
  },

  //API ROUTE
  {
    path: '/',
    component: () => import('layouts/login/index.vue'),
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import('pages/login/index.vue')
      },
    ]
  },
  /* FIN */

  /* INICIO SISTEMA */
  /* INICIO DASHBOARD */
  {
    path: '/',
    component: () => import('layouts/system/system-layout.vue'),
    children: [
      {
        path: 'dashboard',
        name: 'dashboard',
        meta: {
          requiresAuth: true
        },
        component: () => import('pages/system/dashboard/index.vue')
      },
    ]
  },
  {
    path: '/',
    component: () => import('layouts/system/system-layout.vue'),
    children: [
      {
        path: 'clientes',
        name: 'clientes',
        meta: {
          requiresAuth: true
        },
        component: () => import('pages/system/customer/index.vue')
      },
    ]
  },
  /* FIN DASHBOARD */

  /* FIN SISTEMA */

  {
    path: '*',
    redirect: {
      name: 'error'
    }
  },
  {
    path: '/sistema/error',
    name: 'error',
    component: () => import('pages/Error404.vue')
  }
]

export default routes

import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'
import { LocalStorage } from 'quasar'
Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function ({ store /*, ssrContext*/ }) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })
  /*
  Router.beforeEach((to, from, next) => {
    if (to.matched.some(item => item.meta.requiresAuth)) {
      let verificarPermiso = false; 
      if (LocalStorage.getItem('_shp_vrt_tn') && LocalStorage.getItem('_mdls_')) { 
          LocalStorage.getItem('_mdls_').forEach(elemento => { 
            if (elemento.link == to.path) {
              verificarPermiso = true
            }
          })
          if (verificarPermiso) { 
            next()
          } else {
            Router.replace({ name: 'error', params: { valor: '2' } }) //valor 2 de no tiene permisos
          } 
      } else {
        LocalStorage.clear(); 
        Router.push('login')
      }
    } else { 
      if (LocalStorage.getItem('_shp_vrt_tn')) { //esto es para el caso de cuando se quiera acceder al login se direccione a /sistema/slider
        if (to.name == "error") {
          next()
        } else {
          if (to.name=='login'){ 
            Router.replace("/sistema/slider")
          }else{
            next()
          }
        }
      } else {
        next()
      }
    }
  })
  */

  return Router
}

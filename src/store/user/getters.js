export function authGetter(state) {
  return state.user
}

export function tokenGetter(state) {
  return state.token
}

export function modulesGetter(state) {
  if (state.modulos) {
    return state.modulos
  } else {
    return null;
  }
}

export function notificationsGetter(state) {
  if (state.user) {
    return state.user.notifications
  } else {
    return null;
  }
}

export function authIdGetter(state) {
  if (state.user) {
    return state.user.id
  } else {
    return null;
  }
}
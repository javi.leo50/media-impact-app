import { axiosInstance } from 'boot/axios'
// import { i18n } from 'boot/i18n'
import { Notify } from 'quasar'

export async function loginAction({ commit, dispatch, getters }, payload) {
  const rep = await axiosInstance.post('api/login', { ...payload })
    .then(async response => {
      let token = response.data
      Notify.create({
        color: "positive",
        position: "bottom",
        message: "Bienvenido " + token.data.user.full_name,
        icon: "check",
      });
      commit('loginMutation', { token });
      //dispatch('authAction');
    })
  return rep;
}

export async function logoutAction({ commit }, user) {
  axiosInstance.post('api/auth/v1/logout', { ...user })
    .then(() => { commit('logoutMutation') })
}

export async function logoutActionWithInvalidToken({ commit }, user) {
  commit('logoutMutation')
}

export async function authAction(context) {
  let token = context.getters['tokenGetter']
  if (token) {
    const { data } = await axiosInstance.get('api/auth/v1/user')
      .catch(error => {

      });
    context.commit('authMutation', { user: data })
    context.commit('modulosMutation', { user: data })

    return { ...data[0], ...data }
  }
}

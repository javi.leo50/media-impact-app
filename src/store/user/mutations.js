import { Cookies, LocalStorage } from 'quasar'

export function loginMutation(state, data) {
  state.token = data.token.data.access_token
  if (state.cookie) {
    Cookies.set('_shp_vrt_tn', data.token.data.access_token, {
      path: '/',
      expires: '1d'
    })
  }
  else {
    LocalStorage.set('_shp_vrt_tn', data.token.data.access_token);
  }
  this.$router.push("/dashboard");
}

export function modulosMutation(state, user) {
  state.modulos = user.user.data.modules
  if (state.cookie) {
    Cookies.set('_mdls_', user.user.data.modules, {
      path: '/',
      expires: '1d'
    })
  }
  else {
    LocalStorage.set('_mdls_', user.user.data.modules)
  }
}

export function authMutation(state, { user }) {
  state.user = user.data
}

export function logoutMutation(state) {
  state.user = null
  state.token = null
  state.modulos = null
  if (state.cookie) {
    Cookies.remove('_shp_vrt_tn')
    Cookies.remove('_mdls_')
  } else {
    LocalStorage.remove('_shp_vrt_tn')
    LocalStorage.remove('_mdls_')
  }
  // localStorage.removeItem('token')
  // Redirect to login.
  this.$router.push({ name: 'login' })
}

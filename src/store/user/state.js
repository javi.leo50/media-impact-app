import { Cookies, LocalStorage } from 'quasar'
import { cookie } from 'boot/axios'
export default {
  user: null,
  modulos: cookie ? Cookies.get('_mdls_') : LocalStorage.getItem('_mdls_'),
  token: cookie ? Cookies.get('_shp_vrt_tn') : LocalStorage.getItem('_shp_vrt_tn'),
  cookie
}
